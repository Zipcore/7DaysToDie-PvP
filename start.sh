#!/bin/bash
# Script by Tim Bieber - Host-Unlimited.de - Last edited 06.02.2017
cd gameserver
cp serverconfig.xml ../
cd ..
echo ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG 
echo .
echo .
echo .
echo . Der Server wird automatisch bei jedem Start geupdatet
echo .
echo .
echo .
echo ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG ACHTUNG 
echo .
echo .
. logindaten
./steam/steamcmd.sh +login $Benutzername $Passwort +runscript update.txt
mv serverconfig.xml serverconfig-backup.xml
cp serverconfig-backup.xml gameserver
rm serverconfig-backup.xml
cd gameserver
mv serverconfig-backup.xml serverconfig.xml

export LD_LIBRARY_PATH=.

if [ -f serverconfig.xml ]; then
	sed -e 's/  <property name="ServerIP"        value="[0-9.]*"[\/\]>/  <property name="ServerIP"        value="'$1'"\/\>/g' < serverconfig.xml > serverconfig.bak
	sed -e 's/  <property name="ServerPort"        value="[[:digit:]]*"[\/\]>/  <property name="ServerPort"        value="'$2'"\/\>/g' < serverconfig.bak > serverconfig.xml
	sed -e 's/  <property name="ServerMaxPlayerCount"     value="[[:digit:]]*"[\/\]>/  <property name="ServerMaxPlayerCount"     value="'$3'"\/\>/g' < serverconfig.xml > serverconfig.bak
	rm serverconfig.xml
	mv serverconfig.bak serverconfig.xml
fi
cd ..

./gameserver/7DaysToDieServer.x86_64 -configfile=serverconfig.xml -logfile screenlog.0
